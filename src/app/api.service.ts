import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  _contact = 'http://www.planmyhealth.in/planhealthapi/api/contactusform';
  constructor(private httpClient: HttpClient) { }

  contactapi(contactdata) {
    let formData: FormData = new FormData(); 
   formData.append('name', contactdata.name); 
   formData.append('email', contactdata.email); 
   formData.append('phonenumber', contactdata.message); 
   formData.append('message', contactdata.message);   
   return this.httpClient.post<any>(this._contact, formData)
      .pipe(catchError(this.errorHandler))
  }
  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
