import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  errorMsg: any;

  constructor(private formBuilder: FormBuilder,public _api : ApiService) { }

  ContactForm = this.formBuilder.group({
    name: ['', [Validators.required]],
      phonenumber: ['', [Validators.required]],
    email: ['', [Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
    message: ['', [Validators.required]]
  });

  get name() {
    return this.ContactForm.get("name");
  }
  get email() {
    return this.ContactForm.get("email");
  }
  get phonenumber() {
    return this.ContactForm.get("phonenumber");
  }
  get message() {
    return this.ContactForm.get("message");
  }


  public errorMessages = {
    name: [
      { type: 'required', message: 'Email Is Required' },
      { type: "pattern", message: "Enter Valid Email." }
    ],
    email: [
      { type: 'required', message: 'Password Is Required' }
    ],
    phonenumber: [
      { type: 'required', message: 'Email Is Required' },
      { type: "pattern", message: "Enter Valid Email." }
    ],
    message: [
      { type: 'required', message: 'Password Is Required' }
    ]
  };
  ngOnInit(): void {
  }

  public submit() { 
    this._api.contactapi(this.ContactForm.value)
    .subscribe(
       data => {
        if (data['success'] == '1') {
          this.ContactForm.reset();
          alert(data['msg']);
        } else {
          alert(data['msg']);
          this.ContactForm.reset();
        }
      },
      error => { this.errorMsg = error.success }
    )
  }
}
