export class UserOnLinePortal{
        constructor(
          public UserName = '',
          public PhoneNumber = '',
          public EmailId = '',
          public Country ='',
          public Batch = '',
          public Certi?: string,
          public Domain?: string,
          public WorkExperience?: string,
          public CreatedBy = '',
          public CreatedDate?: Date) { }
      
  }